jQuery(document).ready(function() {
	
	var form = jQuery('#formulario-ajax');
	
	jQuery(form).submit(function(e) {

		jQuery('.form-group').removeClass('has-error');
		jQuery('.help-block').remove();

		jQuery.ajax({
			type 		: 'POST',
			url 		: 'procesado.php',
			data 		: form.serialize(),
			dataType 	: 'json',
			encode 		: true
		})
			
		.done(function(data) {
			
			if ( !data.success) {
				
				if (data.errors.nombre) {
					jQuery('#grupo-nombre').addClass('has-error');
					jQuery('#grupo-nombre').append('<div class="help-block">' + data.errors.nombre + '</div>');
				}

				if (data.errors.primerApellido) {
					jQuery('#grupo-apellido-1').addClass('has-error');
					jQuery('#grupo-apellido-1').append('<div class="help-block">' + data.errors.primerApellido + '</div>');
				}
				
				jQuery('#formulario-ajax .alert.alert-success').css('display', 'none');

			} else {
				
				jQuery('#formulario-ajax .alert.alert-success').css('display', 'block');
				
				jQuery('#formulario-ajax .alert.alert-success').html(data.message + '<br />Nombre: ' + data.nombre + ' <br /> Primer apellido: ' + data.primerApellido + ' <br /> Segundo apellido: ' + data.segundoApellido);

			}
			
			console.log(data);
			
		})

		.fail(function(data) {
			
			console.log(data);
			
		});
		
		e.preventDefault();
		
	});

});

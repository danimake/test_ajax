<?php

$errors = array();
$data	= array();

if (empty($_POST['nombre']))
	$errors['nombre'] = 'El nombre es obligatorio.';

if (empty($_POST['primerApellido']))
	$errors['primerApellido'] = 'El primer apellido es obligatorio.';

if ( !empty($errors) ) {

	$data['success'] = false;
	$data['errors']  = $errors;
		
} else {
		
	$data['success'] = true;
	$data['message'] = 'Datos procesados correctamente';
	$data['nombre'] = $_POST['nombre'];
	$data['primerApellido'] = $_POST['primerApellido'];
	$data['segundoApellido'] = $_POST['segundoApellido'];
	 
}

echo json_encode($data);